var config = {

  // Set either one of these
  assetId: '12422608354438203866',
  currencyId: '',

  // Only if no assetId is set
  showAllAssets: false,

  // Only if no currencyId is set
  showAllCurrencies: false,
};
